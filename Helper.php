<?php

/**
 * Contains all usual and required PHP functions
 */

if(!function_exists('assets')){

    function assets($path){
        return "/assets/".$path;
    }

}

if(!function_exists("inc")){

    function inc($path){
        return include_once "../layouts/".$path.'.php';
    }

}