$(document).ready(changeLang('fr'));
$('#textFontMenu').on('click', function () {
    $("#textFontDropup").dropdown('toggle');
});

$('body').on('click', function () {
    $('#textFontMenu').dropdown('dispose');
    $("#bubble").modal('hide');
});

var text = [
    $("p"),
    $("h1"),
    $("h2"),
    $("h3"),
    $("h4"),
    $("h5"),
    $("h6"),
    $("a"),
];

var layout = [
    $(".container-fluid"),
    $("#topNav"),
    $("#utilsNav"),
    $(".card-text"),
    $(".card-body"),
    $("#navOffset")
];

function reloadFonts(){
    var fontURL = $("#textFont option:selected").attr('data-url');
    var fontName = $("#textFont option:selected").val();
    $.each(text, function(index, value){
        value.css('font-family', fontName);
        value.css('src', fontURL);
    });
}

function changeTextSize() {
    var size = $("#textSize").val();
    $.each(text, function(index, value){
        value.css('font-size', size);
    });
}

function changeTextWeight() {
    var weight = $("#textWeight").val();
    $.each(text, function(index, value){
        value.css('font-weight', 400 + (weight-1)*100);
    });
}

function changeTheme(){

    var themeSelector = $("#themeSelector");
    var currentTheme = themeSelector.attr('data-theme');

    if(currentTheme == "light"){

        $.each(layout, function(index, value){
            value.removeClass('bg-light');
            value.addClass('bg-dark');
        });
        $.each(text, function(index, value){
            value.css("color", "#ffffff");
        });
        themeSelector.attr('data-theme', "dark");
        themeSelector.addClass("bg-light");
    }
    else {
        $.each(layout, function(index, value){
            value.addClass('bg-light');
            value.removeClass('bg-dark');
        });
        $.each(text, function(index, value){
            value.css("color", "#000000");
        });
        themeSelector.attr('data-theme', "light");
        themeSelector.removeClass('bg-light');
    }


}

function displayBubble(id) {
        $('#imgEnl').attr('src', $('#img'+id).attr('src'));
        $('#bubble').modal('show');
}

function changeLang(lang){

    var title = $('#title');
    var content = $('#content');
    var search = $("#search");
    var searchInput = $("#searchInput");
    var textSelect = $("#textSelector");
    var themeSelect = $('#themeSelector');
    var utilsText = $('#utilsTitle');
    var langSelect = $('#langSelector');

    if(lang == "en"){
        title.html("How it works ?");
        content.html("Hello Mr. Fouras, you can find the settings down to this page. " +
            "Unless you're now too old to read, write of anything else than ask questions that only you understand," +
            "developers work continously to make Internet and the World Wide Web accessible even for people" +
            "who like to lost everyone with their interrogations ! (Yes, this message is a bit different from the" +
            "french one, where's the interest else ? Père Fouras doesn't know English, no ?... No ?.. <br />" +
        "Nevermind. You can change font settings through the \"Text\" button, and you can tell the Time Masters that " +
            "they won't be blind anymore : White and Dark themes are availables through the \"Theme\" button too !" +
            "If you can't see images, just click on it and they will display larger. Enjoy !");
        textSelect.html('Text');
        themeSelect.html('Theme');
        utilsText.html("Utils");
        search.html("Search");
        searchInput.attr("plceholder", "Search");
        langSelect.html('Language');
    } else {
        title.html("Comment que ça marche ?");
        content.html("Bonjour Père Fouras, vous trouverez l'utilitaire de réglage en bas de page. " +
            "Malgré votre vieil âge, accéder à Internet reste simple grâce à la patience et aux compétences" +
            "des développeurs ! <br /> Vous pouvez changer le thème" +
            "en cliquant sur le bouton \"Thème\", ainsi, les Maîtres du temps pourront enfin " +
            "voir grâce aux thèmes Clair et Sombre ! Pour vos yeux fatigués, vous pouvez" +
            "changer les caractéristiques du texte et " +
            "afficher les images en plus grands en cliquant dessus ! Enjoy !");
        textSelect.html('Texte');
        themeSelect.html('Thème');
        utilsText.html("Utilitaires");
        search.html('Rechercher');
        searchInput.attr('placeholder', 'Rechercher');
        langSelect.html('Langue');
    }

}