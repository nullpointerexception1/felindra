<?php include_once "../Helper.php"; ?>

<html lang="">
    <head>
        <?php inc('header'); ?>
    </head>

    <body>

        <div class="bg-light" id="navOffset" style="margin-top: 65px"></div>
        <div class="container-fluid">
            <audio autoplay="autoplay" loop>
                <source src="<?= assets("music.mp3"); ?>" type="audio/mpeg">
                <source src="<?= assets("music.wav"); ?>" type="audio/wav">
            </audio>
            <?php inc('nav'); ?>

            <h1 class="text-center" id="title">Contenu aléatoire</h1>
            <p class="text-center" id="content">Texte modifiable</p>

            <div class="row col-12 justify-content-around">
                <div class="col-4" style="width: 18rem;">
                    <img src="<?= assets('img/fort1.jfif'); ?>" class="card-img-top" id="img1"
                         alt="Le fort, toujours plus loin, toujours plus, et surtout, toujours plus haut"
                        onclick="displayBubble('1')">
                </div>
                <div class="col-4" style="width: 18rem;">
                    <img src="<?= assets('img/laboule.jpg'); ?>" class="card-img-top" alt="..." id="img2"
                         onclick="displayBubble('2')">
                </div>
                <div class="col-4" style="width: 18rem;">
                    <img src="<?= assets('img/passepartout.jpg'); ?>" class="card-img-top" alt="..." id="img3"
                         onclick="displayBubble('3')">
                </div>
            </div>

            <?php
                for($i = 0; $i < 10; $i++)
                    echo "<br />";
            ?>

        </div>

        <?php inc('nav-utils'); ?>
        <?php inc('footer'); ?>

        <div class="modal fade" id="bubble" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <img src="<?= assets('img/fort1.jfif'); ?>" class="card-img-top" id="imgEnl"
                             alt="Le fort, toujours plus loin, toujours plus, et surtout, toujours plus haut">
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>