<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" id="topNav">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav" aria-controls="nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="nav">
        <a class="navbar-brand" href="#">
            <img src="<?= assets("img/logo_fort_boyard.png"); ?>" width="100" class="d-inline-block align-top" alt="">
        </a>
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0"></ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" id="searchInput">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" id="search">Search</button>
        </form>
    </div>
</nav>