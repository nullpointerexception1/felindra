<?php
return [
    [
        'name' => "Roboto",
        'code' => "roo",
        'url' => "https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu72xKOzY.woff2"
    ],
    [
        'name' => "Solway",
        'code' => "soy",
        'url' => "https://fonts.gstatic.com/s/solway/v1/AMOQz46Cs2uTAOCmhXo8.woff2"
    ],
];