<?php $fonts = inc('fonts'); ?>
<nav class="navbar fixed-bottom navbar-expand-sm bg-light" id="utilsNav">
    <a class="navbar-brand" href="#" id="utilsTitle">Utilitaires</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navUtils" aria-controls="navUtils" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navUtils">
        <ul class="navbar-nav mr-auto"></ul>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <button class="btn btn-outline-dark" id="themeSelector" data-theme="light" onclick="changeTheme()">Theme</button>
            </li>
            <li class="nav-item dropup" id="textFontDropup">
                <a class="nav-link dropdown-toggle"
                   id="textSelector" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Texte
                </a>
                <form class="form-inline dropdown-menu" id="textFontMenu">
                    <div class="form-group" style="margin: 0px 5px">
                        <label for="textFont">Police : &nbsp;</label>
                        <select id="textFont" class="form-control" onchange="reloadFonts()">
                            <?php foreach($fonts as $font): ?>
                                <option value="<?= $font['name'] ?>" data-url="<?= $font['url']; ?>"
                                        style="font-family: <?= $font['name']; ?>
                                                src: url(<?= $font['url']; ?>)">
                                        <?= $font['name']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group" style="margin: 0px 5px">
                        <label for="textSize">Taille : </label>
                        <input type="number" min="5" max="50" value="12"
                               class="form-control" id="textSize" onchange="changeTextSize()" />
                    </div>
                    <div class="form-group" style="margin: 0px 5px">
                        <label for="textWeight">Épaisseur : </label>
                        <input type="number" min="1" max="4" value="1"
                               class="form-control" id="textWeight" onchange="changeTextWeight()" />
                    </div>
                </form>
            </li>
        </ul>
        <ul class="navbar-nav my-2">
            <li class="nav-item dropup">
                <a class="nav-link dropdown-toggle" href="#" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                id="langSelector">
                    Langues
                </a>
                <div class="dropdown-menu" aria-labelledby="lang">
                    <a class="dropdown-item" href="#" onclick="changeLang('fr')"><i class="flag france"></i> Français</a>
                    <a class="dropdown-item" href="#" onclick="changeLang('en')"><i class="united states flag"></i> English</a>
                </div>
            </li>
        </ul>
    </div>
</nav>